# ASP.NET REST API demo #

A demo project on how to build a REST API using ASP.NET Web API 2.

Check out the live demo of the project hosted here: [restapidemosite.azurewebsites.net](http://restapidemosite.azurewebsites.net)

Check out the code examples for an overview of the concepts used in the demo project.

-   [ASP.NET REST API demo](#markdown-header-aspnet-rest-api-demo)
-   [Overview](#markdown-header-overview)
    -   [Running the demo](#markdown-header-running-the-demo)
    -   [Reference links](#markdown-header-reference-links)
-   [Short REST overview](#markdown-header-short-rest-overview)
    -   [REST routes](#markdown-header-rest-routes)
    -   [HTTP request methods](#markdown-header-http-request-methods)
-   [Code examples](#markdown-header-code-examples)
    -   [Controller setup](#markdown-header-controller-setup)
    -   [Versioning](#markdown-header-versioning)
    -   [Caching](#markdown-header-caching)
    -   [Help pages](#markdown-header-help-pages)
    -   [Testing the API](#markdown-header-testing-the-api)

* * *
* * *

# Overview #

The demo shows best practices for developing a REST API, including:

-   API routing using REST model
-   API Versioning
-   ETags and caching
-   Dependency injection
-   Testing the API using mock dependencies
-   Swagger API help page

## Running the demo ##

Get the repo contents and run the Visual Studio solution.
The solution contains the API project and a test project. The solution is self contained and does not require any external data source connection.

## Reference links ##

-   [ASP.NET Web API Guidance](https://docs.microsoft.com/en-us/aspnet/web-api/overview/)
-   [ASP.NET API versioning](https://github.com/Microsoft/aspnet-api-versioning)
-   [CacheCow cashing library](https://github.com/aliostad/CacheCow)
-   [Moq Mocking Library](https://github.com/Moq/moq4/wiki/Quickstart)
-   [Swashbuckle](https://github.com/domaindrivendev/Swashbuckle) for creating swagger help pages

* * *
* * *

# Short REST overview #

## REST routes ##

-   REST urls contain only nouns
-   Nouns map collections of resources
-   individual resources can be accessed as part of a collections
-   Resources can be nested under other resources

| Type | Url | Maps to |
|------|-----|---------|
| Collection | /api/ingredients | A collection of all ingredient resources |
| Resource | /api/ingredients/{ingredient} | The specific ingredient resource (e.g. /api/ingredient/flour) |
| Collection | /api/meals | A collection of all meal resources |
| Resource | /api/meals/{meal} | The specific meal resource (e.g. /api/meals/chili) |
| Nested collection | /api/meals/{meal}/ingredients | A collection of all ingredient resources of the specific meal |

## HTTP request methods ##

-   Actions on the resources are defined by HTTP verbs.
-   See [HTTP request methods](https://tools.ietf.org/html/rfc7231#section-4)

| Type | Example url | GET | HEAD | POST | PUT | PATCH | DELETE |
|------|-------------|-----|------|------|-----|-------|--------|
| Collection | /api/meals | Get collection of all meal resources | Same as GET, but only return headers | Create a new meal resource in the collection | Update (replace) batch of meals | _Not allowed_ | _Not allowed_ |
| Resource | /api/meals/chili | Get specific meal resource | Same as GET, but only return headers | _Not allowed_ | Update (replace) meal | Update parts of the meal resource | Delete the meal resource

-   Some actions are defined to be safe.
    -   Safe means that the client does not expect a server state change to occur because of the request.
-   Some actions are defined to be idempotent.
    -   Idempotency means that if the same request gets submitted multiple times the state of the server is the same as if the request was submitted only once.
    -   Idempotent actions are safe to retry, but non-idempotent are generally not safe to retry.

| Method | Safe | Idempotent |
|--------|------|------------|
| GET | yes | yes |
| HEAD | yes | yes |
| POST | no | no |
| PUT | no | yes |
| PATCH | no | no |
| DELETE | no | yes |

-   HTTP actions have standard response codes that should be followed.

| HTTP action | Scenario | Return message code | Return body |
|-------------|----------|---------------------|-------------|
| GET | Resource found | 200 OK | Resource |
| GET | Resource not found | 404 Not Found | -empty- |
| POST | Resource created successfully | 201 Created | Resource (optional), Location header with link to the new resource |
| POST | Resource already exists | 409 Conflict | -empty- |
| PUT | Resource updated | 200 OK / 204 No Content | Resource / -empty- |
| PATCH | Resource updated | 200 OK / 204 No Content | Resource / -empty- |

* * *
* * *

# Code examples #
-   The following sections provide guidance on how to implement a REST API in ASP.NET Web API projects.
-   The sections contain contain code snippets and examples from the source code.
-   Most of the information can be found in greater detail in the reference links provided above.

* * *

## Controller setup ##

#### Use dependency injection ####
-   Create an interface for the data access layer or any other dependency that the controller has (anything the controller has to construct, really).
-   Add a member variable for each dependency interface.
-   Add a controller constructor that takes in an interface implementation for each dependency.
-   Either use a dependency injection module like Ninject (cool option) or provide a parameterless constructor that creates the dependencies (not so cool, but less effort to set up).
-   When writing tests for the controller, pass in mocks of the dependencies via the parameterized constructor.
-   If several controllers need the same dependencies, consider creating a base class that exposes the dependencies to its child classes.

```csharp
  public class BaseApiController : ApiController
  {
      private IDataAccessLayer _dal;

      protected IDataAccessLayer DataAccessLayer { get { return _dal; } }

      public BaseApiController(IDataAccessLayer dal)
      {
          _dal = dal;
      }

      public BaseApiController()
      {
          _dal = new FakeDataAccessLayer();
      }

  }
```
#### Use data transfer objects (DTO) ####
-   Use DTO objects in the API layer to return to the client instead of using the objects that come from the data access layer.
-   The DTO object contains only the information that is relevant to the API consumer. The data access layer object may contain additional fields that should not be visible or accessible to the client.
-   This also allows the API layer to add metadata (think HATEOAS) and additional information to the DTO objects it returns to the client.
-   To simplify the API layer, it is useful to add methods that can do the conversion from data access layer objects to DTO objects and back.

```csharp
public IHttpActionResult Get(string name)
{
    var ingredient = DataAccessLayer.GetIngredient(name);
    return Ok(ingredient.CreateDto());
}
```

#### Return IHttpActionResult in API methods ####

-   Return `IHttpActionResult` and use `ApiController` methods such as `Ok()`, `BadRequest()`, or `InternalServerError()` to return the appropriate HTTP responses.
-   Since the return type is always `IHttpActionResult`, use the **``ResponseType``** attribute to declare the type that the method will return. This is useful for populating API help pages with proper data.
-   Write unit tests that check the response type of the API method to ensure it returns data in the correct format (see unit test section).

| Scenario | Http Response | IHttpActionResult to return |
|----------|---------------|----------|
| GET success | 200 | `Ok(data)` |
| PUT success | 200 | `Ok(newData)` |
| POST success | 201 | `CreatedAtRoute(...)` |
| DELETE success | 204 | `StatusCode(System.Net.HttpStatusCode.NoContent)` |
| Bad user input | 400 | `BadRequest(message)` |
| Resource not found | 404 | `NotFound()` |
| POST conflict | 409 | `Conflict()` |
| Internal error | 500 | `InternalServerError(details)` |


Example:

```csharp
[ResponseType(typeof(IEnumerable<IngredientDto>))]
public IHttpActionResult Get()
{
    try
    {
        var ingredients = DataAccessLayer.GetIngredients().Select(i => i.CreateDto());

        return Ok(ingredients);
    }
    catch (Exception ex)
    {
        return InternalServerError(ex);
    }
}
```

#### Use CancellationToken in async controller methods ####
-   Add a `CancellationToken` parameter to async controller methods.
-   Pass the token down into any async function that is used.
-   The token gets automatically created. The caller of the API does not know about the parameter.
-   When the client disconnects the token gets canceled which allows the server to stop the async actions.

```csharp
public async Task<IHttpActionResult> Get(int delay, CancellationToken cancellationToken)
{
    await Task.Delay(TimeSpan.FromSeconds(delay), cancellationToken);
    return Ok(delay);
}
```

* * *

## Versioning ##
### Organize versions via namespaces ###
Separate different versions of the controller, model, and data access layer classes by using different namespaces that contain the version number. For example:

-   RestApiDemo.Controllers.**V1**.DelayController
-   RestApiDemo.Controllers.**V2**.DelayController

### Routing with versions ###
For this project, we're using url based versions (like `~/api/`**`v1`**`/{controller}`) instead of other versioning methods such as query based versions (like `~/api/{controller}`**`?version=1.0`**) or header based versions.

#### Setting up versioning ####
-   Use the **`Microsoft.AspNet.WebApi.Versioning`** NuGet package.
-   See [ASP.NET API versioning](https://github.com/Microsoft/aspnet-api-versioning) for documentation and examples.
-   Use the **`[ApiVersion]`** attribute to set the version of the controller or method.
-   Mark deprecated APIs using the `Deprecated = true` option.

```csharp
namespace RestApiDemo.Controllers.V1
{
    [ApiVersion("1.0", Deprecated = true)]
    public class DelaysController : ApiController {}
}
namespace RestApiDemo.Controllers.V2
{
    [ApiVersion("2.0")]
    public class DelaysController : ApiController
}
```

#### Creating routes with versions ####
-   Use attribute based routing.
-   Use the **`[RoutePrefix]`** attribute on a controller to set the base path.
-   The **`[RoutePrefix]`** attribute references the version we set with the **`[ApiVersion]`** attribute.
-   Use the **`[Route]`** attribute on the individual methods.

```csharp
[ApiVersion("1.0")]
[RoutePrefix("api/v{version:apiVersion}/ingredients")]
public class IngredientsController : BaseApiController
{
    [Route("")]
    [ResponseType(typeof(IEnumerable<IngredientDto>))]
    public IHttpActionResult Get()
    {
      ...
    }
}
```
-   Remove the default route configuration since we don't need convention based routing. Comment out the following lines in `WebApiConfig.cs`:

```csharp
public static void Register(HttpConfiguration config)
{
    ...
    //config.Routes.MapHttpRoute(
    //    name: "DefaultApi",
    //    routeTemplate: "api/{controller}/{id}",
    //    defaults: new { id = RouteParameter.Optional }
    //);
    ...
}
```


-   Enable routing for url based versioning. Add the following to the configuration in `WebApiConfig.cs`:

```csharp
public static void Register(HttpConfiguration config)
{
    var constraintResolver = new DefaultInlineConstraintResolver()
    {
        ConstraintMap =
        {
            ["apiVersion"] = typeof( ApiVersionRouteConstraint )
        }
    };
    config.MapHttpAttributeRoutes(constraintResolver);
    config.AddApiVersioning(options =>
    {
        options.ReportApiVersions = true;
    });
    ...
}
```

* * *

## Caching ##

-   Server computes and caches an ETag for each request.
    -   ETag is conceptually like a hash of the response body.
    -   Identical requests will return identical ETags as long as the requested resource hasn't changed on the server.
-   Server includes the ETag as a header in every response.
    -   Weak ETag (starts with **W**, like **ETag: W/"7b4c1a01841444839b492ef74a5c1f48"**): resource cached in memory on the server. ETag won't match other server instances and will go away on server restart.
    -   Hard ETag (like **ETag: "7b4c1a01841444839b492ef74a5c1f48"**): Etag stored in data layer (such as SQL) and will reamin the same until the resou changes in the data layer.
-   Clients can remember the headers (e.g. ETag: "7b4c1a01841444839b492ef74a5c1f48" ) and make the following requests:

| Request | Request Header | Usage | ETag match result | ETag differ result |
|---------|--------|-------|-------------------|--------------------|
| GET   | If-None-Match: "7b4c1a01841444839b492ef74a5c1f48"  | The server will compare the Etag for the request with the ETag in its cache. If the Etags match a response is returned immediately, otherwise the data layer is queried for the resource info. | **304 Not Modified** - Headers only, no message body. | **200 OK** -  Normal GET response with new ETag and resource body |
| PUT   | If-Match: "7b4c1a01841444839b492ef74a5c1f48" | Only allow the PUT request if the ETags match. Otherwise the client has to GET the latest version resource first and PUT the resource with the new ETag returned by the GET request. | **200 OK** - Normal PUT response  | **412 Precondition Failed** - Headers only, no message body. |


### Implementing caching ###

-   Instructions from here: [bitoftech](http://bitoftech.net/2014/02/08/asp-net-web-api-resource-caching-etag-cachecow/)
-   Use the NuGet **CacheCow.Server45** (45 in the name means this is the version for .NET 4.5 or higher, if you are using an older .NET version use **CacheCow.Server**)
    -   Client applications can use **CacheCow.Client45**
-   After installing the NuGet on the server, add these lines to WebApiConfig.cs

```csharp
//Configure HTTP Caching using Entity Tags (ETags)
var cacheCowCacheHandler = new CacheCow.Server.CachingHandler(config);
config.MessageHandlers.Add(cacheCowCacheHandler);
```

-   This will inject a cache handler to the API layer and add an ETag header to every reponse (e.g. ETag: "b260bfe48bd54c30990c30fca5a7df59")
-   It will take care of caching ETags and handling requests like GET and PUT.
-   Additional CacheCow NuGet package exist for different data stores like SqlServer, Redis, MongoDB, etc.

* * *

## Help pages ##

### Swagger help pages ###
-   Check the [Swashbuckle](https://github.com/domaindrivendev/Swashbuckle) documentation
-   Swashbuckle will work in conjuction with `ApiExplorer` to create swagger help pages that let users explore the API.
    -   Swagger pages provide a more extensive help page than the default API help page that is added as part of the Web API template in Visual Studio.
-   The Swagger help page will have an [Open API](https://www.openapis.org/about) endpoint that makes the API discoverable by any service that understands Open API.
    -   [Azure API Management](https://docs.microsoft.com/en-us/azure/api-management/api-management-key-concepts) is an example that uses Open API to discover services.
-   After some initial configuration, the API help pages will be created by using the XML documentation and attributes of the controllers and DTO classes.
-   _**Document your code!**_

#### Configuration ####
-   Add the **`Swashbuckle`** NuGet
-   Under Project -> Properties -> Build -> Output, check the XML documentation file and set it to something like 'App_Data/XmlDocument.xml'.
    -   This will create a file from the XML documentation you added to your classes and methods.
    -   The file will be used to add better metadata to the help pages.
-   Get rid of the `SwaggerConfig.cs` file that came with the `Swashbuckle` Nuget. The configuration has to be done in `WebApiConfig.cs` to tie it together with the `ApiExplorer` configuration.
-   Add the configuration in `WebApiConfig.cs`. Example:

```csharp
public static void Register(HttpConfiguration config)
{
    // configure API explorer for the API help page
    var apiExplorer = config.AddVersionedApiExplorer(options =>
    {
        options.SubstituteApiVersionInUrl = true; // make help page show ~/api/v1/{controller}/ instead of ~/api/{version}/{controller}
    });

    // configure swagger API help page
    config.EnableSwagger(
        "{apiVersion}/swagger",
        swagger =>
        {
            // build a swagger document and endpoint for each discovered API version
            swagger.MultipleApiVersions(
                (apiDescription, version) => apiDescription.GetGroupName() == version,
                info =>
                {
                    foreach (var group in apiExplorer.ApiDescriptions)
                    {
                        var description = "Demo API with versioning and Swagger help pages. View other API versions using the dropdown on the top of the page.";
                        var title = $"Demo API v{group.ApiVersion}";

                        if (group.IsDeprecated)
                        {
                            title += " (deprecated)";
                        }

                        info.Version(group.ApiVersion.ToString(), title)
                            .Description(description);
                    }
                });

            // integrate xml comments
            swagger.IncludeXmlComments(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, @"App_Data\XmlDocument.xml"));
        })
    .EnableSwaggerUi(
        swagger =>
        {
            // Shows a dropwdown with urls for all versions of the API.
            swagger.EnableDiscoveryUrlSelector();

            // Specify which HTTP operations will have the 'Try it out!' option. An empty paramter list disables
            // it for all operations.
            swagger.SupportedSubmitMethods("GET", "HEAD");
        }
    );
    ...
}
```
-   Add a link to the swagger help page (`~/swagger`) somewhere. For example in the `_Layout.cshtml` navbar.

```html
<ul class="nav navbar-nav">
    <li class="nav-item">@Html.ActionLink("Home", "Index", "Home", new { area = "" }, new { @class = "nav-link" })</li>
    <li class="nav-item">@Html.ActionLink("API", "Index", "Help", new { area = "" }, new { @class = "nav-link" })</li>
    <li class="nav-item"><a class="nav-link" href="~/swagger">Swagger</a></li>
</ul>
```

-   Document the code using Swagger annotation attributes.
    -   Add a `[SwaggerOperation("operationId")]` attribute to each API method to specify what the **unique** operationId should be.
    -   If you don't manually speficy the operation ID then Swagger will autogenerate one, but this can lead to trouble. For example if you have `GET /api/ingredients` and `GET /api/ingredients/{id}`, then Swagger will assign both operations the operationId `Ingredients_Get`. This collission causes failures when services try to parse the Open API documentation of the API.

* * *

## Testing the API ##
-   Use a test framework like **`MSTest`** to test the API controllers.

### Mock controller dependencies ###

-   Use a mocking library like **`Moq`**. See [Moq documentation](https://github.com/Moq/moq4/wiki/Quickstart).
-   Mock any dependency the controller has.
-   Configure the needed mock functions of the dependency.
-   Construct the API controller using the mock dependency.

```csharp
[TestMethod]
public void GetIngredients()
{
    // Create mock objects
    var mockDal = new Mock<IDataAccessLayer>();
    mockDal.Setup(x => x.GetIngredients())
           .Returns(new List<Ingredient>
           {
               new Ingredient { Id = 0, Name = "a" },
               new Ingredient { Id = 1, Name = "b" },
               new Ingredient { Id = 2, Name = "Rice"},
               new Ingredient { Id = 3, Name = "Beans" }
           });

    // Create controller using mock objects
    IngredientsController controller = new IngredientsController(mockDal.Object);
    ...
}
```

### Test for proper HTTP responses ###

-   Cast the response from the controller to the expected result.

| Server response | Server returns | Test checks for |
|----------|----------------|-----------------|
| 200 OK | `Ok()` | `OkNegotiatedContentResult<T>` |
| 201 Created | `CreatedAtRoute()` | `CreatedAtRouteNegotiatedContentResult` |
| 400 Bad Request | `BadRequest("message")` | `BadRequestErrorMessageResult` |
| 409 Conflict | `Conflict()` | `ConflictResult` |
| 500 InternalServerError | `InternalServerError()` | `ExceptionResult` |
| ... | ... | ... |

-   Assert the result is not null and then check for correctness of result.
    -   E.g. for `CreatedAtRouteNegotiatedContentResult` check that the route of the created resource in the response header is correct.

```csharp
[TestMethod]
public void PostIngredient()
{
    ...
    // post proper model
    var ingredientName = "a";
    var postModel = new IngredientDto { Name = ingredientName, Id = 123 };
    var result = controller.Post(postModel) as CreatedAtRouteNegotiatedContentResult<IngredientDto>;
    Assert.IsNotNull(result);
    Assert.AreEqual(ingredientName, result.RouteValues["name"]);
    Assert.IsTrue(result.Content.Name.Equals(ingredientName, StringComparison.Ordinal));
    Assert.AreEqual(mockId, result.Content.Id);

    // post model again causing a conflict
    var conflictResult = controller.Post(postModel) as ConflictResult;
    Assert.IsNotNull(conflictResult);

    // post bad models
    var badInputResult = controller.Post(null) as BadRequestErrorMessageResult;
    Assert.IsNotNull(badInputResult);

    badInputResult = controller.Post(new IngredientDto { Id = 12 }) as BadRequestErrorMessageResult;
    Assert.IsNotNull(badInputResult);
}
```
