﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestApiDemo.Controllers.V1;
using RestApiDemo.DAL.V1;
using RestApiDemo.DAL.V1.Models;
using RestApiDemo.DTO.V1;

namespace RestApiDemo.Tests.Controllers.V1
{
    [TestClass]
    public class MealsControllerTest
    {
        [TestMethod]
        public void GetMeals()
        {
            // Create mock objects
            var mockDal = new Mock<IDataAccessLayer>();
            mockDal.Setup(x => x.GetMeals())
                   .Returns(new List<Meal>
                   {
                        new Meal { Id = 0, Name = "Chili", Ingredients = new List<Ingredient> { new Ingredient { Id = 0, Name = "a" }, new Ingredient { Id = 1, Name = "b" } } },
                        new Meal { Id = 1, Name = "Rice and Beans", Ingredients = new List<Ingredient> { new Ingredient { Id = 2, Name = "Rice"}, new Ingredient { Id = 3, Name = "Beans" } } },
                   });

            // Create controller using mock objects
            MealsController controller = new MealsController(mockDal.Object);

            // Act
            var result = controller.Get() as OkNegotiatedContentResult<IEnumerable<MealDto>>;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Content.Count());
            var chili = result.Content.SingleOrDefault(m => m.Name.Equals("Chili", StringComparison.Ordinal));
            var riceAndBeans = result.Content.SingleOrDefault(m => m.Name.Equals("Rice and Beans", StringComparison.Ordinal));
            Assert.IsNotNull(chili);
            Assert.IsNotNull(riceAndBeans);
            Assert.AreEqual(2, riceAndBeans.Ingredients.Count());
            Assert.IsNotNull(riceAndBeans.Ingredients.SingleOrDefault(i => i.Name.Equals("Rice", StringComparison.Ordinal)));
        }


        [TestMethod]
        public void GetMealByName()
        {
            var mealName = "chili";

            // Create mock objects
            var mockDal = new Mock<IDataAccessLayer>();
            mockDal.Setup(x => x.GetMeal(mealName))
                   .Returns(new Meal { Name = mealName, Ingredients = new List<Ingredient> { new Ingredient { Id = 0, Name = "a" }, new Ingredient { Id = 1, Name = "b" } } });

            // Create controller using mock objects
            MealsController controller = new MealsController(mockDal.Object);

            // Get meal that exists
            var result = controller.Get(mealName) as OkNegotiatedContentResult<MealDto>;
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
            Assert.IsTrue(result.Content.Name.Equals(mealName, StringComparison.Ordinal));
            Assert.IsTrue(result.Content.Ingredients.Count() == 2);

            // Get meal that doesn't exist
            var notFoundResult = controller.Get("chili2") as NotFoundResult;
            Assert.IsNotNull(notFoundResult);
        }

        [TestMethod]
        public void DeleteMeal()
        {
            var mealName = "chili";
            bool isDeleted = false;

            // Create mock objects
            var mockDal = new Mock<IDataAccessLayer>();

            // comfigure mock to return the meal if it hasn't been deleted.
            mockDal.Setup(x => x.GetMeal(It.IsAny<string>()))
                   .Returns((string name) => isDeleted ? null : new Meal { Name = name, Ingredients = new List<Ingredient> { new Ingredient { Name = "a" } } });

            // configure mock to mark meal as deleted
            mockDal.Setup(x => x.DeleteMeal(mealName))
                   .Callback(() => isDeleted = true);

            // Create controller using mock objects
            MealsController controller = new MealsController(mockDal.Object);

            // Get mealName that exists
            var result = controller.Get(mealName) as OkNegotiatedContentResult<MealDto>;
            Assert.IsNotNull(result);

            // Delete mealName
            var deletedResult = controller.Delete(mealName) as StatusCodeResult;
            Assert.IsNotNull(deletedResult);
            Assert.AreEqual(System.Net.HttpStatusCode.NoContent, deletedResult.StatusCode);

            // Get deleted mealName
            var notFoundResult = controller.Get(mealName) as NotFoundResult;
            Assert.IsNotNull(notFoundResult);

            // Delete mealName again
            var errorResult = controller.Delete(mealName) as NotFoundResult;
            Assert.IsNotNull(errorResult);
        }

        [TestMethod]
        public void PutMeal_BadData()
        {
            string mealName = "chili";
            var mockDal = new Mock<IDataAccessLayer>();
            MealsController controller = new MealsController(mockDal.Object);

            // Put without data
            var badResult = controller.Put(mealName, null) as BadRequestErrorMessageResult;
            Assert.IsNotNull(badResult);

            // Put empty meal
            badResult = controller.Put(mealName, new MealDto()) as BadRequestErrorMessageResult;
            Assert.IsNotNull(badResult);

            // Put meal with no ingredients
            badResult = controller.Put(mealName, new MealDto { Name = mealName } ) as BadRequestErrorMessageResult;
            Assert.IsNotNull(badResult);

            // Put meal with no name
            badResult = controller.Put(mealName, new MealDto { Ingredients = new List<IngredientDto> { new IngredientDto { Name = "a" } } } ) as BadRequestErrorMessageResult;
            Assert.IsNotNull(badResult);

            // Put meal with wrong name
            badResult = controller.Put(mealName, new MealDto { Name = "wrongName", Ingredients = new List<IngredientDto> { new IngredientDto { Name = "a" } } }) as BadRequestErrorMessageResult;
            Assert.IsNotNull(badResult);
        }

        [TestMethod]
        public void PutMeal_NotExist()
        {
            string mealName = "chili";
            string ingredientName = "salt";

            var mockDal = new Mock<IDataAccessLayer>();
            mockDal.Setup(x => x.GetIngredient(ingredientName))
                   .Returns(new Ingredient { Name = ingredientName, Id = 0 });
            mockDal.Setup(x => x.GetMeal(mealName))
                   .Returns(new Meal { Name = mealName, Ingredients = new List<Ingredient> { new Ingredient { Name = ingredientName, Id = 0 } } });
            MealsController controller = new MealsController(mockDal.Object);

            // Put for meal that does not exist
            var badMealName = "a";
            var notFoundResult = controller.Put(badMealName, new MealDto { Name = badMealName, Ingredients = new List<IngredientDto> { new IngredientDto { Name = ingredientName } } }) as NotFoundResult;
            Assert.IsNotNull(notFoundResult);

            // Put with ingredient that does not exist
            var badResult = controller.Put(mealName, new MealDto { Name = mealName, Ingredients = new List<IngredientDto> { new IngredientDto { Name = "a" } } }) as BadRequestErrorMessageResult;
            Assert.IsNotNull(badResult);
        }
    }
}
