﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestApiDemo.Controllers.V1;
using RestApiDemo.DAL.V1;
using RestApiDemo.DAL.V1.Models;
using RestApiDemo.DTO.V1;

namespace RestApiDemo.Tests.Controllers.V1
{
    [TestClass]
    public class IngredientsControllerTest
    {
        [TestMethod]
        public void GetIngredients()
        {
            // Create mock objects
            var mockDal = new Mock<IDataAccessLayer>();
            mockDal.Setup(x => x.GetIngredients())
                   .Returns(new List<Ingredient>
                   {
                       new Ingredient { Id = 0, Name = "a" },
                       new Ingredient { Id = 1, Name = "b" },
                       new Ingredient { Id = 2, Name = "Rice"},
                       new Ingredient { Id = 3, Name = "Beans" }
                   });

            // Create controller using mock objects
            IngredientsController controller = new IngredientsController(mockDal.Object);

            // Act
            var result = controller.Get() as OkNegotiatedContentResult<IEnumerable<IngredientDto>>;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Content.Count());
            var beans = result.Content.SingleOrDefault(i => i.Name.Equals("Beans", StringComparison.Ordinal));
            Assert.IsNotNull(beans);
            Assert.AreEqual(3, beans.Id);
        }


        [TestMethod]
        public void GetIngredientByName()
        {
            var ingredientName = "Garlic";

            // Create mock objects
            var mockDal = new Mock<IDataAccessLayer>();
            mockDal.Setup(x => x.GetIngredient(ingredientName))
                   .Returns(new Ingredient { Id = 0, Name = ingredientName });

            // Create controller using mock objects
            IngredientsController controller = new IngredientsController(mockDal.Object);

            // Get ingredient that exists
            var result = controller.Get(ingredientName) as OkNegotiatedContentResult<IngredientDto>;
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
            Assert.IsTrue(result.Content.Name.Equals(ingredientName, StringComparison.Ordinal));
            Assert.AreEqual(0, result.Content.Id);

            // Get ingredient that doesn't exist
            var notFoundResult = controller.Get("Onion") as NotFoundResult;
            Assert.IsNotNull(notFoundResult);
        }


        [TestMethod]
        public void PostIngredient()
        {
            int mockId = 17;
            var addedNames = new List<string>();
            var mockDal = new Mock<IDataAccessLayer>();

            // configure mock to return a model with specific ID on Add and add the name to a list of added ingredients
            mockDal.Setup(x => x.AddIngredient(It.IsAny<Ingredient>()))
                   .Callback((Ingredient i) => addedNames.Add(i.Name))
                   .Returns((Ingredient i) => new Ingredient { Id = mockId, Name = i.Name });

            // configure mock to return an ingredient if the ingredient name is in the list of added ingredients
            mockDal.Setup(x => x.GetIngredient(It.IsAny<string>()))
                   .Returns((string s) => addedNames.Contains(s) ? new Ingredient { Id = mockId, Name = s } : null);

            IngredientsController controller = new IngredientsController(mockDal.Object);

            // post proper model
            var ingredientName = "a";
            var postModel = new IngredientDto { Name = ingredientName, Id = 123 };
            var result = controller.Post(postModel) as CreatedAtRouteNegotiatedContentResult<IngredientDto>;
            Assert.IsNotNull(result);
            Assert.AreEqual(ingredientName, result.RouteValues["name"]);
            Assert.IsTrue(result.Content.Name.Equals(ingredientName, StringComparison.Ordinal));
            Assert.AreEqual(mockId, result.Content.Id);

            // post model again causing a conflict
            var conflictResult = controller.Post(postModel) as ConflictResult;
            Assert.IsNotNull(conflictResult);

            // post bad models
            var badInputResult = controller.Post(null) as BadRequestErrorMessageResult;
            Assert.IsNotNull(badInputResult);

            badInputResult = controller.Post(new IngredientDto { Id = 12 }) as BadRequestErrorMessageResult;
            Assert.IsNotNull(badInputResult);
        }

        [TestMethod]
        public void DeleteIngredient()
        {
            var ingredientName = "Garlic";
            bool isDeleted = false;

            // Create mock objects
            var mockDal = new Mock<IDataAccessLayer>();

            // comfigure mock to return ingredient if it hasn't been deleted.
            mockDal.Setup(x => x.GetIngredient(It.IsAny<string>()))
                   .Returns((string name) => isDeleted ? null : new Ingredient { Id = 0, Name = name });

            // configure mock to mark ingredient as deleted
            mockDal.Setup(x => x.DeleteIngredient(ingredientName))
                   .Callback(() => isDeleted = true);

            // Create controller using mock objects
            IngredientsController controller = new IngredientsController(mockDal.Object);

            // Get ingredient that exists
            var result = controller.Get(ingredientName) as OkNegotiatedContentResult<IngredientDto>;
            Assert.IsNotNull(result);

            // Delete ingredient
            var deletedResult = controller.Delete(ingredientName) as StatusCodeResult;
            Assert.IsNotNull(deletedResult);
            Assert.AreEqual(System.Net.HttpStatusCode.NoContent, deletedResult.StatusCode);

            // Get deleted ingredient
            var notFoundResult = controller.Get(ingredientName) as NotFoundResult;
            Assert.IsNotNull(notFoundResult);

            // Delete ingredient again
            var errorResult = controller.Delete(ingredientName) as NotFoundResult;
            Assert.IsNotNull(errorResult);
        }
    }
}
