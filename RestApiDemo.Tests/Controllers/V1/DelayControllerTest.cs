﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestApiDemo.Controllers.V1;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace RestApiDemo.Tests.Controllers.V1
{
    [TestClass]
    public class DelayControllerTest
    {
        [TestMethod]
        public async Task GetDelay()
        {
            DelaysController controller = new DelaysController();

            // delay for non-negative durations
            var delayTimes = new List<int> { 2, 0 };
            foreach (int secondsToDelay in delayTimes ) {
                var result = await controller.Get(secondsToDelay, new CancellationToken()) as OkNegotiatedContentResult<int>;
                Assert.IsNotNull(result);
                Assert.AreEqual(secondsToDelay, result.Content);
            }

            // delay for negative duration
            delayTimes = new List<int> { -1 };
            foreach (int secondsToDelay in delayTimes)
            {
                var badResult = await controller.Get(secondsToDelay, new CancellationToken()) as BadRequestErrorMessageResult;
                Assert.IsNotNull(badResult);
            }
        }

        [TestMethod]
        public async Task GetDelayAndCancel()
        {
            int secondsToDelayInController = 5;
            DelaysController controller = new DelaysController();

            // Act
            var source = new CancellationTokenSource();
            source.CancelAfter(TimeSpan.FromMilliseconds(200)); // set token cancellation time to before GET call will return
            var result = await controller.Get(secondsToDelayInController, source.Token);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ExceptionResult));
        }
    }
}
