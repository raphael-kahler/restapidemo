﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestApiDemo.Controllers.V2;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace RestApiDemo.Tests.Controllers.V2
{
    [TestClass]
    public class DelayControllerTest
    {
        [TestMethod]
        public async Task GetDelay()
        {
            DelaysController controller = new DelaysController();

            // delay for non-negative durations
            var delaysInMilliseconds = new List<int> { 2000, 25, 0 };
            foreach (var delay in delaysInMilliseconds)
            {
                var result = await controller.Get(delay, new CancellationToken()) as OkNegotiatedContentResult<int>;
                Assert.IsNotNull(result);
                Assert.AreEqual(delay, result.Content);
            }

            // delay for negative duration
            delaysInMilliseconds = new List<int> { -100 };
            foreach (var delay in delaysInMilliseconds)
            {
                var badResult = await controller.Get(delay, new CancellationToken()) as BadRequestErrorMessageResult;
                Assert.IsNotNull(badResult);
            }
        }

        [TestMethod]
        public async Task GetDelayAndCancel()
        {
            var delayInMilliseconds = 5000;
            DelaysController controller = new DelaysController();

            // Act
            var source = new CancellationTokenSource();
            source.CancelAfter(TimeSpan.FromMilliseconds(200)); // set token cancellation time to before GET call will return
            var result = await controller.Get(delayInMilliseconds, source.Token);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ExceptionResult));
        }
    }
}
