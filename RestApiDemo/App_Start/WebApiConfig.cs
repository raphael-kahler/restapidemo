﻿using Microsoft.Web.Http.Routing;
using Newtonsoft.Json.Serialization;
using Swashbuckle.Application;
using System.IO;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace RestApiDemo
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // configure versioning to use url path versioning (e.g. ~/api/v1/{controller}) instead of query parameter versioning (e.g. ~/api/{controller}?version=1.0)
            var constraintResolver = new DefaultInlineConstraintResolver()
            {
                ConstraintMap =
                {
                    ["apiVersion"] = typeof( ApiVersionRouteConstraint )
                }
            };
            config.MapHttpAttributeRoutes(constraintResolver);
            config.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
            });

            // configure API explorer for the API help page
            var apiExplorer = config.AddVersionedApiExplorer(options =>
            {
                options.SubstituteApiVersionInUrl = true; // make help page show ~/api/v1/{controller}/ instead of ~/api/{version}/{controller}
            });

            // configure swagger API help page
            config.EnableSwagger(
                "{apiVersion}/swagger",
                swagger =>
                {
                    // build a swagger document and endpoint for each discovered API version
                    swagger.MultipleApiVersions(
                        (apiDescription, version) => apiDescription.GetGroupName() == version,
                        info =>
                        {
                            foreach (var group in apiExplorer.ApiDescriptions)
                            {
                                var description = "Demo API with versioning and Swagger help pages. View other API versions using the dropdown on the top of the page.";
                                var title = $"Demo API v{group.ApiVersion}";

                                if (group.IsDeprecated)
                                {
                                    title += " (deprecated)";
                                }

                                info.Version(group.ApiVersion.ToString(), title)
                                    .Description(description);
                            }
                        });

                    // integrate xml comments
                    swagger.IncludeXmlComments(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, @"App_Data\XmlDocument.xml"));
                })
            .EnableSwaggerUi(
                swagger =>
                {
                    // Shows a dropwdown with urls for all versions of the API.
                    swagger.EnableDiscoveryUrlSelector();

                    // Specify which HTTP operations will have the 'Try it out!' option. An empty paramter list disables
                    // it for all operations.
                    swagger.SupportedSubmitMethods("GET", "HEAD");
                }
            );


            // We're not using convention based routing in this project. Instead we are using the attribute routing inside of the controller classes.
            // This means we can leave the default routing convention below commented out in this project.
            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);

            // configure json formatter to camelCase properties
            var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            json.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            // turn off xml formatter
            var xml = GlobalConfiguration.Configuration.Formatters.XmlFormatter;
            xml.SupportedMediaTypes.Clear();

            //Configure HTTP Caching using Entity Tags (ETags)
            var cacheCowCacheHandler = new CacheCow.Server.CachingHandler(config);
            config.MessageHandlers.Add(cacheCowCacheHandler);
        }
    }
}
