﻿using System.Collections.Generic;
using RestApiDemo.DAL.V1.Models;

namespace RestApiDemo.DAL.V1
{
    public interface IDataAccessLayer
    {
        IEnumerable<Meal> GetMeals();
        Meal GetMeal(string id);
        Meal AddMeal(Meal meal);
        Meal UpdateMeal(Meal meal);
        void DeleteMeal(string id);

        IEnumerable<Ingredient> GetIngredients();
        Ingredient GetIngredient(string id);
        Ingredient AddIngredient(Ingredient ingredient);
        Ingredient UpdateIngredient(Ingredient ingredient);
        void DeleteIngredient(string id);
    }
}