﻿using System;

namespace RestApiDemo.DAL.V1.Models
{
    public class Ingredient
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}