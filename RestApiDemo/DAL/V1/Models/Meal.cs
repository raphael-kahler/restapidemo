﻿using System.Collections.Generic;

namespace RestApiDemo.DAL.V1.Models
{
    public class Meal
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<Ingredient> Ingredients { get; set; }
    }
}