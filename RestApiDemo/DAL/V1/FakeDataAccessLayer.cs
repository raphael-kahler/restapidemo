﻿using RestApiDemo.DAL.V1.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RestApiDemo.DAL.V1
{
    public class FakeDataAccessLayer : IDataAccessLayer
    {
        private static IList<Meal> _meals;
        private static IList<Ingredient> _ingredients;
        private static int _mealIdCount;
        private static int _ingredientIdCount;

        static FakeDataAccessLayer()
        {
            // initialize a data store in static variables.
            _ingredients = new List<Ingredient>
            {
                new Ingredient { Id = 0, Name = "Flour" },
                new Ingredient { Id = 1, Name = "Water" },
                new Ingredient { Id = 2, Name = "Yeast" },
                new Ingredient { Id = 3, Name = "Butter" },
                new Ingredient { Id = 4, Name = "Sugar" },
                new Ingredient { Id = 5, Name = "Ground Beef" },
                new Ingredient { Id = 6, Name = "Tomatoes" },
                new Ingredient { Id = 7, Name = "Kidney Beans" },
            };
            _ingredientIdCount = _ingredients.Count;

            _meals = new List<Meal>
            {
                new Meal
                {
                    Id = 0,
                    Name = "Bread",
                    Ingredients = new List<Ingredient>
                    {
                        _ingredients.Single(i => i.Name.Equals("Flour", StringComparison.InvariantCultureIgnoreCase)),
                        _ingredients.Single(i => i.Name.Equals("Water", StringComparison.InvariantCultureIgnoreCase)),
                        _ingredients.Single(i => i.Name.Equals("Yeast", StringComparison.InvariantCultureIgnoreCase)),
                    }
                },
                new Meal
                {
                    Id = 1,
                    Name = "Chili",
                    Ingredients = new List<Ingredient>
                    {
                        _ingredients.Single(i => i.Name.Equals("Ground Beef", StringComparison.InvariantCultureIgnoreCase)),
                        _ingredients.Single(i => i.Name.Equals("Tomatoes", StringComparison.InvariantCultureIgnoreCase)),
                        _ingredients.Single(i => i.Name.Equals("Kidney Beans", StringComparison.InvariantCultureIgnoreCase)),
                    }
                },
            };
            _mealIdCount = _meals.Count;
        }

        public IEnumerable<Meal> GetMeals()
        {
            return _meals;
        }

        public Meal GetMeal(string name)
        {
            return _meals.SingleOrDefault(m => m.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        public Meal AddMeal(Meal meal)
        {
            var ingredients = new List<Ingredient>();
            foreach (var ingredient in meal.Ingredients)
            {
                try
                {
                    ingredients.Add(_ingredients.Single(i => i.Name.Equals(ingredient.Name, StringComparison.InvariantCultureIgnoreCase)));
                }
                catch (InvalidOperationException)
                {
                    throw new ArgumentException($"Meal contains non-existent ingredient '{ingredient.Name}'. Add the ingredient first.");
                }
            }
            var newMeal = new Meal
            {
                Id = _mealIdCount++,
                Ingredients = ingredients,
                Name = meal.Name
            };
            _meals.Add(newMeal);
            return GetMeal(meal.Name);
        }

        public Meal UpdateMeal(Meal meal)
        {
            var oldMeal = _meals.Single(m => m.Name.Equals(meal.Name, StringComparison.InvariantCultureIgnoreCase));
            var ingredients = new List<Ingredient>();
            foreach (var ingredient in meal.Ingredients)
            {
                try
                {
                    ingredients.Add(_ingredients.Single(i => i.Name.Equals(ingredient.Name, StringComparison.InvariantCultureIgnoreCase)));
                }
                catch (InvalidOperationException)
                {
                    throw new ArgumentException($"Meal contains non-existent ingredient '{ingredient.Name}'. Add the ingredient first.");
                }
            }
            var newMeal = new Meal
            {
                Id = oldMeal.Id,
                Ingredients = ingredients,
                Name = meal.Name
            };
            _meals.Remove(oldMeal);
            _meals.Add(newMeal);
            return newMeal;
        }

        public void DeleteMeal(string id)
        {
            _meals.Remove(_meals.Single(m => m.Name.Equals(id, StringComparison.InvariantCultureIgnoreCase)));
        }

        public IEnumerable<Ingredient> GetIngredients()
        {
            return _ingredients;
        }

        public Ingredient GetIngredient(string name)
        {
            return _ingredients.SingleOrDefault(i => i.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        public Ingredient AddIngredient(Ingredient ingredient)
        {
            Ingredient newIngredient = new Ingredient
            {
                Id = _ingredientIdCount++,
                Name = ingredient.Name
            };
            _ingredients.Add(newIngredient);
            return GetIngredient(newIngredient.Name);
        }

        public Ingredient UpdateIngredient(Ingredient ingredient)
        {
            throw new System.NotImplementedException();
        }

        public void DeleteIngredient(string id)
        {
            _ingredients.Remove(_ingredients.Single(i => i.Name.Equals(id, StringComparison.InvariantCultureIgnoreCase)));
        }
    }
}