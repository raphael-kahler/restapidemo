﻿using System.ComponentModel.DataAnnotations;

namespace RestApiDemo.DTO.V1
{
    /// <summary>
    /// A representation of the ingredient resource.
    /// </summary>
    public class IngredientDto
    {
        /// <summary>
        /// The ID of the ingredient.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The url of the ingredient.
        /// </summary>
        public string Href { get; set; }

        /// <summary>
        /// The name of the ingredient.
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name has to be provided.")]
        [Key]
        public string Name { get; set; }
    }
}