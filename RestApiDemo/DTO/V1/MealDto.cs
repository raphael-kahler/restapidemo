﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RestApiDemo.DTO.V1
{
    /// <summary>
    /// A representation of the meal resource.
    /// </summary>
    public class MealDto
    {
        /// <summary>
        /// The Id of the meal.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The url of the meal.
        /// </summary>
        public string Href { get; set; }

        /// <summary>
        /// The name of the meal.
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name has to be provided.")]
        [Key]
        public string Name { get; set; }

        /// <summary>
        /// The ingredients that make up the meal.
        /// </summary>
        public IEnumerable<IngredientDto> Ingredients { get; set; }
    }
}