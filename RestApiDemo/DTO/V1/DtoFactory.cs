﻿using RestApiDemo.DAL.V1.Models;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Routing;

namespace RestApiDemo.DTO.V1
{
    /// <summary>
    /// Class used for converting to and from DTO objects.
    /// Parsing functionality is exposed as extension methods.
    /// </summary>
    public static class DtoFactory
    {
        /// <summary>
        /// Create a DTO ingredient from an ingredient.
        /// </summary>
        /// <param name="ingredient">The ingredient.</param>
        /// <param name="request">The http request message.</param>
        /// <returns>The DTO for that ingredient.</returns>
        public static IngredientDto CreateDto(this Ingredient ingredient, HttpRequestMessage request)
        {
            return new IngredientDto
            {
                Id = ingredient.Id,
                Href = request == null ? string.Empty : new UrlHelper(request).Link("Ingredients_GetSingle", new { name = ingredient.Name }),
                Name = ingredient.Name
            };
        }

        /// <summary>
        /// Create an ingredient from a DTO ingredient.
        /// </summary>
        /// <param name="ingredientDto">The DTO ingredient.</param>
        /// <returns>The ingredient for that DTO.</returns>
        public static Ingredient ParseFromDto(this IngredientDto ingredientDto)
        {
            return new Ingredient
            {
                Name = ingredientDto.Name
            };
        }

        /// <summary>
        /// Create a DTO meal from a meal.
        /// </summary>
        /// <param name="meal">The meal.</param>
        /// <param name="request">The http request message.</param>
        /// <returns>The DTO for that meal.</returns>
        public static MealDto CreateDto(this Meal meal, HttpRequestMessage request)
        {
            return new MealDto
            {
                Id = meal.Id,
                Href = request == null ? string.Empty : new UrlHelper(request).Link("Meals_GetSingle", new { name = meal.Name }),
                Name = meal.Name,
                Ingredients = meal.Ingredients.Select(i => i.CreateDto(request))
            };
        }

        /// <summary>
        /// Create a meal from a DTO meal.
        /// </summary>
        /// <param name="mealDto">The DTO meal.</param>
        /// <returns>The meal for that DTO.</returns>
        public static Meal ParseFromDto(this MealDto mealDto)
        {
            return new Meal
            {
                Name = mealDto.Name,
                Ingredients = mealDto.Ingredients.Select(i => i.ParseFromDto())
            };
        }
    }
}