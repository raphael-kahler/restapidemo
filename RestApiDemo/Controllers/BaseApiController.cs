﻿using RestApiDemo.DAL.V1;
using System.Web.Http;

namespace RestApiDemo.Controllers
{
    public class BaseApiController : ApiController
    {
        private IDataAccessLayer _dal;

        protected IDataAccessLayer DataAccessLayer
        {
            get
            {
                return _dal;
            }
        }

        public BaseApiController(IDataAccessLayer dal)
        {
            _dal = dal;
        }

        public BaseApiController()
        {
            _dal = new FakeDataAccessLayer();
        }

    }
}
