﻿using Microsoft.Web.Http;
using RestApiDemo.DAL.V1;
using RestApiDemo.DTO.V1;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace RestApiDemo.Controllers.V1
{
    /// <summary>
    /// API controller to serve meal data.
    /// </summary>
    [ApiVersion("1.0")]
    [RoutePrefix("api/v{version:apiVersion}/meals")]
    public class MealsController : BaseApiController
    {
        public MealsController() { }
        public MealsController(IDataAccessLayer dal) : base (dal) { }

        /// <summary>
        /// Get all meals.
        /// </summary>
        /// <returns>All meals.</returns>
        [Route("")]
        [SwaggerOperation("Meals_GetAll")]
        [ResponseType(typeof(IEnumerable<MealDto>))]
        public IHttpActionResult Get()
        {
            try
            {
                var meals = DataAccessLayer.GetMeals().Select(m => m.CreateDto(Request));

                return Ok(meals);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        /// <summary>
        /// Create a new meal.
        /// </summary>
        /// <param name="meal">The meal that will be created.</param>
        /// <returns>The newly created meal.</returns>
        [Route("")]
        [SwaggerOperation("Meals_CreateNew")]
        [ResponseType(typeof(MealDto))]
        public IHttpActionResult Post([FromBody] MealDto meal)
        {
            if (null == meal)
            {
                return BadRequest("Meal data in request was malformed or incomplete.");
            }
            if (null == meal.Name)
            {
                return BadRequest("Meal name in data is null.");
            }
            if (null == meal.Ingredients || meal.Ingredients.Count() == 0)
            {
                return BadRequest("Meal data contains no ingredients.");
            }
            try
            {
                // Checking beforehand if the data is already present is not safe against race conditions.
                // The data layer should throw an error if the new element can't be added because it already exists,
                // and the controller should handle that exception properly to return a 400 error.
                if (null != DataAccessLayer.GetMeal(meal.Name))
                {
                    return BadRequest($"A meal with the name '{meal.Name}' already exists.");
                }
                foreach (var ingredient in meal.Ingredients)
                {
                    if (null == DataAccessLayer.GetIngredient(ingredient.Name)) ;
                    {
                        return BadRequest($"Meal contains non-existent ingredient '{ingredient.Name}'. Add the ingredient first.");
                    }
                }
                var createdMeal = DataAccessLayer.AddMeal(meal.ParseFromDto()).CreateDto(Request);
                return CreatedAtRoute("Meals_GetSingle", new { name = createdMeal.Name }, createdMeal);
            }
            catch(ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Get a meal with the specified name.
        /// </summary>
        /// <param name="name">The meal name.</param>
        /// <returns>The meal with the specified name.</returns>
        [Route("{name}", Name = "Meals_GetSingle")]
        [SwaggerOperation("Meals_GetSingle")]
        [ResponseType(typeof(MealDto))]
        public IHttpActionResult Get(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return BadRequest("Meal name was not specified.");
            }

            try
            {
                var meal = DataAccessLayer.GetMeal(name);

                if (null == meal)
                {
                    return NotFound();
                }
                return Ok(meal.CreateDto(Request));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Delete the meal with the specified name.
        /// </summary>
        /// <param name="name">The meal name.</param>
        /// <returns>An OK result if the meal was deleted successfully.</returns>
        [Route("{name}")]
        [SwaggerOperation("Meals_Delete")]
        [ResponseType(null)]
        public IHttpActionResult Delete(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return BadRequest("Meal name was not specified.");
            }
            try
            {
                if (null == DataAccessLayer.GetMeal(name))
                {
                    return NotFound();
                }
                DataAccessLayer.DeleteMeal(name);
                return StatusCode(System.Net.HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Update the meal with the specified name.
        /// </summary>
        /// <param name="name">The name of the meal.</param>
        /// <param name="meal">The updated content of the meal.</param>
        /// <returns>The updated meal.</returns>
        [Route("{name}")]
        [SwaggerOperation("Meals_Update")]
        [ResponseType(typeof(MealDto))]
        public IHttpActionResult Put(string name, [FromBody] MealDto meal)
        {
            if (null == meal)
            {
                return BadRequest("Meal data in request was malformed or incomplete.");
            }
            if (null == meal.Name)
            {
                return BadRequest("Meal name in data is null.");
            }
            if (!meal.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase))
            {
                return BadRequest($"Meal name in data is '{meal.Name}' which does not match resource name '{name}'.");
            }
            if (null == meal.Ingredients || meal.Ingredients.Count() == 0)
            {
                return BadRequest("Meal data contains no ingredients.");
            }
            try
            {
                if (null == DataAccessLayer.GetMeal(meal.Name))
                {
                    return NotFound();
                }
                foreach (var ingredient in meal.Ingredients)
                {
                    if (null == DataAccessLayer.GetIngredient(ingredient.Name))
                    {
                        return BadRequest($"Meal contains non-existent ingredient '{ingredient.Name}'. Add the ingredient first.");
                    }
                }
                var newMeal = DataAccessLayer.UpdateMeal(meal.ParseFromDto());
                return Ok(newMeal.CreateDto(Request));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
