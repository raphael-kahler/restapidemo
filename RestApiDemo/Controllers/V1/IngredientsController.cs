﻿using Microsoft.Web.Http;
using RestApiDemo.DAL.V1;
using RestApiDemo.DTO.V1;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace RestApiDemo.Controllers.V1
{
    /// <summary>
    /// API controller to serve ingredient data.
    /// </summary>
    [ApiVersion("1.0")]
    [RoutePrefix("api/v{version:apiVersion}/ingredients")]
    public class IngredientsController : BaseApiController
    {
        public IngredientsController() { }
        public IngredientsController(IDataAccessLayer dal) : base (dal) { }

        /// <summary>
        /// Get all ingredients.
        /// </summary>
        /// <returns>All ingredients.</returns>
        [Route("", Name = "Ingredients_GetAll")]
        [SwaggerOperation("Ingredients_GetAll")]
        [ResponseType(typeof(IEnumerable<IngredientDto>))]
        public IHttpActionResult Get()
        {
            try
            {
                var ingredients = DataAccessLayer.GetIngredients().Select(i => i.CreateDto(Request));

                return Ok(ingredients);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Create a new ingredient.
        /// </summary>
        /// <param name="ingredient">The ingredient that will be created.</param>
        /// <returns>The newly created ingredient.</returns>
        [Route("")]
        [ResponseType(typeof(IngredientDto))]
        [SwaggerOperation("Ingredients_CreateNew")]
        public IHttpActionResult Post([FromBody] IngredientDto ingredient)
        {
            if (null == ingredient)
            {
                return BadRequest("Ingredient data in request was malformed or incomplete.");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (null == ingredient.Name)
            {
                return BadRequest("Ingredient name not supplied.");
            }
            try
            {
                // Checking beforehand if the data is already present is not safe against race conditions.
                // The data layer should throw an error if the new element can't be added because it already exists,
                // and the controller should handle that exception properly to return a 400 error.
                if (null != DataAccessLayer.GetIngredient(ingredient.Name))
                {
                    return Conflict();
                }
                var createdIngredient = DataAccessLayer.AddIngredient(ingredient.ParseFromDto());
                if (null == createdIngredient)
                {
                    return InternalServerError();
                }
                return CreatedAtRoute("Ingredients_GetSingle", new { name = createdIngredient.Name }, createdIngredient.CreateDto(Request));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Get an ingredient with the specified name.
        /// </summary>
        /// <param name="name">The ingredient name.</param>
        /// <returns>The ingredient with the specified name.</returns>
        [Route("{name}", Name = "Ingredients_GetSingle")]
        [SwaggerOperation("Ingredients_GetSingle")]
        [ResponseType(typeof(IngredientDto))]
        public IHttpActionResult Get(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return BadRequest("Ingredient name was not specified.");
            }

            try
            {
                var ingredient = DataAccessLayer.GetIngredient(name);

                if (null == ingredient)
                {
                    return NotFound();
                }
                return Ok(ingredient.CreateDto(Request));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Delete the ingredient with the specified name.
        /// </summary>
        /// <param name="name">The ingredient name.</param>
        /// <returns>An OK result if the ingredient was deleted successfully.</returns>
        [Route("{name}")]
        [SwaggerOperation("Ingredients_Delete")]
        [ResponseType(null)]
        public IHttpActionResult Delete(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return BadRequest("Ingredient name was not specified.");
            }
            try
            {
                if (null == DataAccessLayer.GetIngredient(name))
                {
                    return NotFound();
                }
                DataAccessLayer.DeleteIngredient(name);
                return StatusCode(System.Net.HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
