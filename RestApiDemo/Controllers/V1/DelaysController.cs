﻿using Microsoft.Web.Http;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace RestApiDemo.Controllers.V1
{
    /// <summary>
    /// API controller used to perform delay operations.
    /// Version 1.0 of the implementation uses int to count seconds in a time span.
    /// </summary>
    [ApiVersion("1.0", Deprecated = true)]
    [RoutePrefix("api/v{version:apiVersion}/delays")]
    public class DelaysController : ApiController
    {
        /// <summary>
        /// Delay for a specified number of seconds, then return the delay time as number of seconds.
        /// </summary>
        /// <param name="delay">The number of seconds to delay.</param>
        /// <param name="cancellationToken">A cancellation token that gets cancelled when the client disconnects.</param>
        /// <returns>The duration in seconds that was delayed.</returns>
        [Route("{delay:int}", Name = "Delays_Get_V1")]
        [SwaggerOperation("Delays_Get")]
        [ResponseType(typeof(int))]
        public async Task<IHttpActionResult> Get(int delay, CancellationToken cancellationToken)
        {
            if (delay < 0)
            {
                return BadRequest("Delay time has to be non-negative.");
            }
            try
            {
                await Task.Delay(TimeSpan.FromSeconds(delay), cancellationToken);
                return Ok(delay);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
