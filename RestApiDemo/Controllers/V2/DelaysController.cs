﻿using Microsoft.Web.Http;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace RestApiDemo.Controllers.V2
{
    /// <summary>
    /// API controller used to perform delay operations.
    /// Version 2.0 of the implementation uses int to count milliseconds in a time span.
    /// </summary>
    [ApiVersion("2.0")]
    [RoutePrefix("api/v{version:apiVersion}/delays")]
    public class DelaysController : ApiController
    {
        /// <summary>
        /// Delay for a specified number of milliseconds, then return the delay time as number of milliseconds.
        /// </summary>
        /// <param name="delay">The number of milliseconds to delay.</param>
        /// <param name="cancellationToken">A cancellation token that gets cancelled when the client disconnects.</param>
        /// <returns>The duration in miiliseconds that was delayed.</returns>
        [Route("{delay}", Name = "Delays_Get_V2")]
        [SwaggerOperation("Delays_Get")]
        [ResponseType(typeof(int))]
        public async Task<IHttpActionResult> Get(int delay, CancellationToken cancellationToken)
        {
            if (delay < 0)
            {
                return BadRequest("Delay time has to be non-negative.");
            }
            try
            {
                await Task.Delay(TimeSpan.FromMilliseconds(delay), cancellationToken);
                return Ok(delay);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
